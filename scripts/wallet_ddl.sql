CREATE TABLE paisahero.wallet (
  `id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `main_balance` double DEFAULT NULL,
  `msisdn` varchar(255) DEFAULT NULL,
  `promotional_balance` double DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `client_details_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK2lk699m1hf0i3s04wrhke3d4d` (`user_id`,`client_details_id`),
  KEY `FKg7qoqhfln6suae2eiyohnn76q` (`client_details_id`)
); 

CREATE TABLE paisahero.transaction (
  `id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `main_amount` double DEFAULT NULL,
  `promotional_amount` double DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `wallet_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtfwlfspv2h4wcgc9rjd1658a6` (`wallet_id`)
);

CREATE TABLE paisahero.paytm_gratify_response (
  `id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `meta_data` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `request_guid` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status_code` varchar(255) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `gratification_request_id_fk` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKluomk5ktk2jgrkmidu9v2txkb` (`gratification_request_id_fk`)
); 


CREATE TABLE paisahero.hibernate_sequence (
  `next_val` bigint(20) DEFAULT NULL
);

CREATE TABLE paisahero.gratification_request (
  `id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `amount` double NOT NULL,
  `gratification_status` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `meta_data` varchar(255) DEFAULT NULL,
  `msisdn` varchar(255) DEFAULT NULL,
  `response` varchar(2000) DEFAULT NULL,
  `service_charge` double NOT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `wallet_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKav7y05v9s42et4c4idwmkpg0m` (`transaction_id`),
  KEY `FK722bfs7yrnqt2lgg3r6jx9gcp` (`wallet_id`)
);

CREATE TABLE paisahero.client_details (
  `id` bigint(20) NOT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `client_key` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `paisahero`.`hibernate_sequence`
(`next_val`)
VALUES
(0);

INSERT INTO `paisahero`.`client_details`
(`id`,
`authorities`,
`client_id`,
`client_key`,
`roles`,
`secret`)
VALUES
(1,
null,
'paisaheroappclient',
'paisahero',
null,
'paisaheroappsecret');